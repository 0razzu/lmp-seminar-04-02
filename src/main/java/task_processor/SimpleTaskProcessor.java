package task_processor;


import buffer.IBuffer;


public class SimpleTaskProcessor<T extends IBuffer<?>> implements ITaskProcessor {
    private T buffer;
    
    
    public SimpleTaskProcessor(T buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        
        this.buffer = buffer;
    }
    
    
    @Override
    public Integer process() {
        if (buffer.size() == 0)
            return null;
        
        int sum = 0;
        
        for (int number: buffer.get().getData())
            sum += number;
        
        return sum;
    }
}
