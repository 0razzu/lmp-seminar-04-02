package task_processor;


public interface ITaskProcessor {
    Integer process();
}
