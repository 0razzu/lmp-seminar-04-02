package task;


import java.util.Arrays;
import java.util.Objects;


public class PriorityTask implements ITask, Comparable<PriorityTask> {
    private int[] data;
    private int priority;
    
    
    public PriorityTask(int priority, int... data) {
        this.priority = priority;
        this.data = data;
    }
    
    
    public int getPriority() {
        return priority;
    }
    
    
    @Override
    public int[] getData() {
        return data;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PriorityTask)) return false;
        PriorityTask that = (PriorityTask) o;
        return priority == that.priority;
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(priority);
    }
    
    
    @Override
    public int compareTo(PriorityTask o) {
        return priority - o.priority;
    }
}
