package task_generator;


import buffer.IBuffer;
import task.PriorityTask;


public class PriorityTaskGenerator implements ITaskGenerator {
    private IBuffer<PriorityTask> buffer;
    private int startValue;
    private int amount;
    private int priority;
    
    
    public PriorityTaskGenerator(IBuffer<PriorityTask> buffer, int startValue, int amount, int priority) {
        setBuffer(buffer);
        setStartValue(startValue);
        setAmount(amount);
        setPriority(priority);
    }
    
    
    private void setBuffer(IBuffer<PriorityTask> buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        
        this.buffer = buffer;
    }
    
    
    private void setStartValue(int startValue) {
        this.startValue = startValue;
    }
    
    
    private void setAmount(int amount) {
        if (amount < 0)
            throw new IllegalArgumentException("Amount cannot be negative");
        
        this.amount = amount;
    }
    
    
    private void setPriority(int priority) {
        this.priority = priority;
    }
    
    
    public PriorityTaskGenerator withStartValue(int startValue) {
        setStartValue(startValue);
        
        return this;
    }
    
    
    public PriorityTaskGenerator withAmount(int amount) {
        setAmount(amount);
        
        return this;
    }
    
    
    public PriorityTaskGenerator withPriority(int priority) {
        setPriority(priority);
        
        return this;
    }
    
    
    @Override
    public void generate() {
        int[] data = new int[amount];
        
        for (int i = 0; i < amount; i++)
            data[i] = startValue + i;
        
        buffer.add(new PriorityTask(priority, data));
    }
}
