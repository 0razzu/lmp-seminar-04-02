package task_generator;


import buffer.IBuffer;
import task.Task;


public class SimpleTaskGenerator implements ITaskGenerator {
    private IBuffer<Task> buffer;
    private int startValue;
    private int amount;
    
    
    public SimpleTaskGenerator(IBuffer<Task> buffer, int startValue, int amount) {
        setBuffer(buffer);
        setStartValue(startValue);
        setAmount(amount);
    }
    
    
    private void setBuffer(IBuffer<Task> buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        
        this.buffer = buffer;
    }
    
    
    private void setStartValue(int startValue) {
        this.startValue = startValue;
    }
    
    
    private void setAmount(int amount) {
        if (amount < 0)
            throw new IllegalArgumentException("Amount cannot be negative");
        
        this.amount = amount;
    }
    
    
    public SimpleTaskGenerator withStartValue(int startValue) {
        setStartValue(startValue);
        
        return this;
    }
    
    
    public SimpleTaskGenerator withAmount(int amount) {
        setAmount(amount);
        
        return this;
    }
    
    
    @Override
    public void generate() {
        int[] data = new int[amount];
        
        for (int i = 0; i < amount; i++)
            data[i] = startValue + i;
        
        buffer.add(new Task(data));
    }
}
