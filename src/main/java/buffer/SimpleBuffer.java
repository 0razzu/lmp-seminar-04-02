package buffer;


import task.Task;

import java.util.LinkedList;
import java.util.Queue;


public class SimpleBuffer implements IBuffer<Task> {
    private Queue<Task> tasks;
    
    
    public SimpleBuffer() {
        this.tasks = new LinkedList<>();
    }
    
    
    @Override
    public void add(Task task) {
        tasks.add(task);
    }
    
    
    @Override
    public Task get() {
        return tasks.remove();
    }
    
    
    @Override
    public int size() {
        return tasks.size();
    }
    
    
    @Override
    public boolean isEmpty() {
        return tasks.isEmpty();
    }
    
    
    @Override
    public void clear() {
        tasks.clear();
    }
}
