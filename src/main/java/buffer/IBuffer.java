package buffer;


import task.ITask;


public interface IBuffer<T extends ITask> {
    void add(T task);
    ITask get();
    
    int size();
    boolean isEmpty();
    
    void clear();
}
