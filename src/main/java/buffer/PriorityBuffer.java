package buffer;


import task.ITask;
import task.PriorityTask;

import java.util.PriorityQueue;
import java.util.Queue;


public class PriorityBuffer implements IBuffer<PriorityTask> {
    private Queue<PriorityTask> tasks;
    
    
    public PriorityBuffer() {
        this.tasks = new PriorityQueue<>();
    }
    
    
    @Override
    public void add(PriorityTask task) {
        tasks.add(task);
    }
    
    
    @Override
    public ITask get() {
        return tasks.remove();
    }
    
    
    @Override
    public int size() {
        return tasks.size();
    }
    
    
    @Override
    public boolean isEmpty() {
        return tasks.isEmpty();
    }
    
    
    @Override
    public void clear() {
        tasks.clear();
    }
}
