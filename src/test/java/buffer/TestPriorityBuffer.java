package buffer;


import org.junit.jupiter.api.Test;
import task.ITask;
import task.PriorityTask;

import static org.junit.jupiter.api.Assertions.*;


public class TestPriorityBuffer {
    @Test
    void testPriorityBuffer() {
        IBuffer<PriorityTask> buffer1 = new PriorityBuffer();
        PriorityBuffer buffer2 = new PriorityBuffer();
        
        assertAll(
                () -> assertTrue(buffer1.isEmpty()),
                () -> assertTrue(buffer2.isEmpty()),
                () -> assertEquals(0, buffer1.size()),
                () -> assertEquals(0, buffer2.size())
        );
    }
    
    
    @Test
    void testAdd() {
        ITask task1 = new PriorityTask(1);
        PriorityTask task2 = new PriorityTask(63, -2, -3, 0, 25);
        PriorityTask task3 = new PriorityTask(63, 7);
        ITask task4 = new PriorityTask(0, 1);
        PriorityTask task5 = new PriorityTask(-2, 18, 8, -1);
        
        IBuffer<PriorityTask> buffer = new PriorityBuffer();
        
        assertAll(
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
        
        buffer.add((PriorityTask) task1);
        buffer.add(task2);
        buffer.add(task3);
        
        assertAll(
                () -> assertFalse(buffer.isEmpty()),
                () -> assertEquals(3, buffer.size())
        );
        
        buffer.add((PriorityTask) task4);
        buffer.add(task5);
        
        assertAll(
                () -> assertFalse(buffer.isEmpty()),
                () -> assertEquals(5, buffer.size()),
                () -> assertEquals(task5, buffer.get()),
                () -> assertEquals(task4, buffer.get()),
                () -> assertEquals(task1, buffer.get()),
                () -> assertEquals(task2, buffer.get()),
                () -> assertEquals(task3, buffer.get()),
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
    }
    
    
    @Test
    void testClear() {
        PriorityTask task1 = new PriorityTask(8);
        ITask task2 = new PriorityTask(1);
        PriorityTask task3 = new PriorityTask(63, -2, -3, 0, 25);
    
        PriorityBuffer buffer = new PriorityBuffer();
        
        buffer.add(task1);
        buffer.add((PriorityTask) task2);
        buffer.add(task3);
        
        buffer.clear();
        
        assertAll(
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
    }
}
