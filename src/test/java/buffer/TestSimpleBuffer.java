package buffer;


import org.junit.jupiter.api.Test;
import task.*;

import static org.junit.jupiter.api.Assertions.*;


public class TestSimpleBuffer {
    @Test
    void testSimpleBuffer() {
        IBuffer<Task> buffer1 = new SimpleBuffer();
        SimpleBuffer buffer2 = new SimpleBuffer();
        
        assertAll(
                () -> assertTrue(buffer1.isEmpty()),
                () -> assertTrue(buffer2.isEmpty()),
                () -> assertEquals(0, buffer1.size()),
                () -> assertEquals(0, buffer2.size())
        );
    }
    
    
    @Test
    void testAdd() {
        Task task1 = new Task();
        ITask task2 = new Task(1);
        Task task3 = new Task(63, -2, -3, 0, 25);
        Task task4 = new Task(63, -2, -3, 0, 25);
        
        IBuffer<Task> buffer = new SimpleBuffer();
        
        assertAll(
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
        
        buffer.add(task1);
        buffer.add((Task) task2);
        buffer.add(task3);
    
        assertAll(
                () -> assertFalse(buffer.isEmpty()),
                () -> assertEquals(3, buffer.size())
        );
        
        buffer.add(task4);
        
        assertAll(
                () -> assertFalse(buffer.isEmpty()),
                () -> assertEquals(4, buffer.size()),
                () -> assertEquals(task1, buffer.get()),
                () -> assertEquals(task2, buffer.get()),
                () -> assertEquals(task3, buffer.get()),
                () -> assertEquals(task4, buffer.get()),
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
    }
    
    
    @Test
    void testClear() {
        Task task1 = new Task();
        ITask task2 = new Task(1);
        Task task3 = new Task(63, -2, -3, 0, 25);
    
        SimpleBuffer buffer = new SimpleBuffer();
    
        buffer.add(task1);
        buffer.add((Task) task2);
        buffer.add(task3);
        
        buffer.clear();
    
        assertAll(
                () -> assertTrue(buffer.isEmpty()),
                () -> assertEquals(0, buffer.size())
        );
    }
}
