package task;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestTask {
    @Test
    void testTask() {
        int[] emptyData = new int[0];
        int[] data5 = {1, 0, 15, -19, 3, 6, 7};
        
        ITask task1 = new Task();
        Task task2 = new Task(1);
        Task task3 = new Task(3, -72, 3, 0, 1298);
        Task task4 = new Task(emptyData);
        Task task5 = new Task(data5);
        
        assertAll(
                () -> assertArrayEquals(emptyData, task1.getData()),
                () -> assertArrayEquals(new int[]{1}, task2.getData()),
                () -> assertArrayEquals(new int[]{3, -72, 3, 0, 1298}, task3.getData()),
                () -> assertArrayEquals(emptyData, task4.getData()),
                () -> assertArrayEquals(data5, task5.getData())
        );
    }
    
    
    @Test
    void testEquals() {
        int[] data5 = {1, 0, 15, -19, 3, 6, 7};
    
        Task task1 = new Task();
        Task task2 = new Task();
        Task task3 = new Task(1);
        Task task4 = new Task(63, -2, -3, 0, 25);
        Task task5 = new Task(data5);
        ITask task6 = new Task(data5);
        Task task7 = new Task(1, 0, 15, -19, 3, 6, 0);
        
        assertAll(
                () -> assertNotEquals(null, task1),
                () -> assertNotEquals("", task1),
                () -> assertEquals(task1, task1),
                () -> assertEquals(task1, task2),
                () -> assertNotEquals(task2, task3),
                () -> assertNotEquals(task2, task4),
                () -> assertNotEquals(task3, task4),
                () -> assertEquals(task5, task6),
                () -> assertNotEquals(task6, task7)
        );
    }
}
