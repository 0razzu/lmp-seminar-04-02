package task;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestPriorityTask {
    @Test
    void testPriorityTask() {
        int[] emptyData = new int[0];
        int[] data5 = {1, 0, 15, -19, 3, 6, 7};
        
        ITask task1 = new PriorityTask(0);
        PriorityTask task2 = new PriorityTask(1);
        PriorityTask task3 = new PriorityTask(3, -72, 3, 0, 1298);
        PriorityTask task4 = new PriorityTask(10, emptyData);
        PriorityTask task5 = new PriorityTask(-1, data5);
        
        assertAll(
                () -> assertArrayEquals(emptyData, task1.getData()),
                () -> assertArrayEquals(emptyData, task2.getData()),
                () -> assertArrayEquals(new int[]{-72, 3, 0, 1298}, task3.getData()),
                () -> assertArrayEquals(emptyData, task4.getData()),
                () -> assertArrayEquals(data5, task5.getData()),
                () -> assertEquals(0, ((PriorityTask) task1).getPriority()),
                () -> assertEquals(1, task2.getPriority()),
                () -> assertEquals(3, task3.getPriority()),
                () -> assertEquals(10, task4.getPriority()),
                () -> assertEquals(-1, task5.getPriority())
        );
    }
    
    
    @Test
    void testEquals() {
        int[] data5 = {1, 0, 15, -19, 3, 6, 7};
        
        PriorityTask task1 = new PriorityTask(0);
        PriorityTask task2 = new PriorityTask(1);
        PriorityTask task3 = new PriorityTask(1);
        PriorityTask task4 = new PriorityTask(63, -2, -3, 0, 25);
        PriorityTask task5 = new PriorityTask(-1, data5);
        ITask task6 = new PriorityTask(-1, data5);
        PriorityTask task7 = new PriorityTask(1, data5);
        PriorityTask task8 = new PriorityTask(1, 0, 15, -19, 3, 6, 0);
        Task task9 = new Task(data5);
        
        assertAll(
                () -> assertNotEquals(null, task1),
                () -> assertNotEquals("", task1),
                () -> assertEquals(task1, task1),
                () -> assertNotEquals(task1, task2),
                () -> assertEquals(task2, task3),
                () -> assertNotEquals(task2, task4),
                () -> assertEquals(task2, task7),
                () -> assertNotEquals(task3, task4),
                () -> assertEquals(task5, task6),
                () -> assertNotEquals(task6, task7),
                () -> assertNotEquals(task6, task8),
                () -> assertNotEquals(task6, task9)
        );
    }
    
    
    @Test
    void testCompareTo() {
        int[] data5 = {1, 0, 15, -19, 3, 6, 7};
    
        PriorityTask task1 = new PriorityTask(0);
        PriorityTask task2 = new PriorityTask(1);
        PriorityTask task3 = new PriorityTask(63, -2, -3, 0, 25);
        PriorityTask task4 = new PriorityTask(-1, data5);
        ITask task5 = new PriorityTask(-1, data5);
        PriorityTask task6 = new PriorityTask(10, data5);
        PriorityTask task7 = new PriorityTask(-1, 0, 15, -19, 3, 6, 0);
        
        assertAll(
                () -> assertEquals(0, task1.compareTo(task1)),
                () -> assertTrue(task1.compareTo(task2) < 0),
                () -> assertTrue(task1.compareTo(task4) > 0),
                () -> assertTrue(task2.compareTo(task1) > 0),
                () -> assertTrue(task2.compareTo(task3) < 0),
                () -> assertEquals(0, task4.compareTo((PriorityTask) task5)),
                () -> assertTrue(task4.compareTo(task6) < 0),
                () -> assertEquals(0, task4.compareTo(task7))
        );
    }
}
