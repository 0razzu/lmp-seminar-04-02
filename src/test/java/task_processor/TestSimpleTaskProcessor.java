package task_processor;


import buffer.PriorityBuffer;
import buffer.SimpleBuffer;
import org.junit.jupiter.api.Test;
import task.PriorityTask;
import task.Task;
import task_generator.PriorityTaskGenerator;

import static org.junit.jupiter.api.Assertions.*;


public class TestSimpleTaskProcessor {
    @Test
    void testSimpleTaskProcessor() {
        assertDoesNotThrow(() -> new SimpleTaskProcessor<>(new SimpleBuffer()));
        assertDoesNotThrow(() -> new SimpleTaskProcessor<>(new PriorityBuffer()));
        Exception e = assertThrows(NullPointerException.class, () -> new SimpleTaskProcessor<>(null));
        
        assertEquals("Null buffer", e.getMessage());
    }
    
    
    @Test
    void testProcessEmptyBuffer() {
        SimpleTaskProcessor<SimpleBuffer> processor1 = new SimpleTaskProcessor<>(new SimpleBuffer());
        SimpleTaskProcessor<PriorityBuffer> processor2 = new SimpleTaskProcessor<>(new PriorityBuffer());
        
        assertAll(
                () -> assertNull(processor1.process()),
                () -> assertNull(processor2.process())
        );
    }
    
    
    @Test
    void testProcessOneTask() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskProcessor<SimpleBuffer> processor = new SimpleTaskProcessor<>(buffer);
        
        buffer.add(new Task(1, -1, 10));
        
        assertAll(
                () -> assertEquals(10, processor.process()),
                () -> assertNull(processor.process())
        );
    }
    
    
    @Test
    void testProcessTwoTasks() {
        PriorityBuffer buffer = new PriorityBuffer();
        SimpleTaskProcessor<PriorityBuffer> processor = new SimpleTaskProcessor<>(buffer);
        
        buffer.add(new PriorityTask(1, -1, 10));
        new PriorityTaskGenerator(buffer, -5, 11, -1).generate();
        
        assertAll(
                () -> assertEquals(0, processor.process()),
                () -> assertEquals(9, processor.process()),
                () -> assertNull(processor.process())
        );
    }
}
