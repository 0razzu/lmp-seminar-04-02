package task_generator;


import buffer.PriorityBuffer;
import buffer.SimpleBuffer;
import org.junit.jupiter.api.Test;
import task.PriorityTask;
import task.Task;

import static org.junit.jupiter.api.Assertions.*;


public class TestPriorityTaskGenerator {
    @Test
    void testSimpleTaskGenerator1() {
        PriorityBuffer buffer = new PriorityBuffer();
        
        assertDoesNotThrow(() -> new PriorityTaskGenerator(buffer, 0, 1, 1));
        Exception e1 = assertThrows(NullPointerException.class, () -> new PriorityTaskGenerator(null, 0, 1, 1));
        Exception e2 = assertThrows(IllegalArgumentException.class, () -> new PriorityTaskGenerator(buffer, 0, -1, 1));
        
        assertAll(
                () -> assertEquals("Null buffer", e1.getMessage()),
                () -> assertEquals("Amount cannot be negative", e2.getMessage())
        );
    }
    
    
    @Test
    void testSimpleTaskGenerator2() {
        PriorityBuffer buffer = new PriorityBuffer();
        PriorityTaskGenerator generator = new PriorityTaskGenerator(buffer, 1, 2, 2);
        
        assertDoesNotThrow(() -> generator.withStartValue(2).withAmount(3).withPriority(-1));
        Exception e = assertThrows(IllegalArgumentException.class, () -> generator.withStartValue(2).withAmount(-2));
        assertEquals("Amount cannot be negative", e.getMessage());
    }
    
    
    @Test
    void testGenerate1() {
        PriorityBuffer buffer = new PriorityBuffer();
        
        new PriorityTaskGenerator(buffer, 1, 0, 1).generate();
        
        assertArrayEquals(new int[]{}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerate2() {
        PriorityBuffer buffer = new PriorityBuffer();
        ITaskGenerator generator = new PriorityTaskGenerator(buffer, 1, 2, 2);
        
        generator.generate();
        
        assertArrayEquals(new int[]{1, 2}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerate3() {
        PriorityBuffer buffer = new PriorityBuffer();
        
        new PriorityTaskGenerator(buffer, -5, 11, -2).generate();
        
        assertArrayEquals(new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithStartValue() {
        PriorityBuffer buffer = new PriorityBuffer();
        ITaskGenerator generator = new PriorityTaskGenerator(buffer, 1, 2, 0);
        
        ((PriorityTaskGenerator) generator).withStartValue(5).generate();
        
        assertArrayEquals(new int[]{5, 6}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithAmount() {
        PriorityBuffer buffer = new PriorityBuffer();
        PriorityTaskGenerator generator = new PriorityTaskGenerator(buffer, 0, 1, 10);
        
        generator.withAmount(5).generate();
        
        assertArrayEquals(new int[]{0, 1, 2, 3, 4}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithPriority() {
        PriorityBuffer buffer = new PriorityBuffer();
        PriorityTaskGenerator generator = new PriorityTaskGenerator(buffer, 0, 1, 10);
        
        generator.withPriority(5).generate();
        
        assertArrayEquals(new int[]{0}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithEverything() {
        PriorityBuffer buffer = new PriorityBuffer();
        
        new PriorityTaskGenerator(buffer, 1, 1, 1).withStartValue(-2).withAmount(5).withPriority(100).generate();
        
        assertArrayEquals(new int[]{-2, -1, 0, 1, 2}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithNonEmptyBuffer() {
        PriorityTask task = new PriorityTask(0, 0, 0);
        PriorityBuffer buffer = new PriorityBuffer();
        ITaskGenerator generator = new PriorityTaskGenerator(buffer, 0, 3, -2);
        
        buffer.add(task);
        
        generator.generate();
        
        assertAll(
                () -> assertArrayEquals(new int[]{0, 1, 2}, buffer.get().getData()),
                () -> assertArrayEquals(task.getData(), buffer.get().getData())
        );
    }
}
