package task_generator;


import buffer.SimpleBuffer;
import org.junit.jupiter.api.Test;
import task.Task;

import static org.junit.jupiter.api.Assertions.*;


public class TestSimpleTaskGenerator {
    @Test
    void testSimpleTaskGenerator1() {
        SimpleBuffer buffer = new SimpleBuffer();
        
        assertDoesNotThrow(() -> new SimpleTaskGenerator(buffer, 0, 1));
        Exception e1 = assertThrows(NullPointerException.class, () -> new SimpleTaskGenerator(null, 0, 1));
        Exception e2 = assertThrows(IllegalArgumentException.class, () -> new SimpleTaskGenerator(buffer, 0, -1));
        
        assertAll(
                () -> assertEquals("Null buffer", e1.getMessage()),
                () -> assertEquals("Amount cannot be negative", e2.getMessage())
        );
    }
    
    
    @Test
    void testSimpleTaskGenerator2() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 2);
        
        assertDoesNotThrow(() -> generator.withStartValue(2).withAmount(3));
        Exception e = assertThrows(IllegalArgumentException.class, () -> generator.withStartValue(2).withAmount(-2));
        assertEquals("Amount cannot be negative", e.getMessage());
    }
    
    
    @Test
    void testGenerate1() {
        SimpleBuffer buffer = new SimpleBuffer();
        
        new SimpleTaskGenerator(buffer, 1, 0).generate();
        
        assertArrayEquals(new int[]{}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerate2() {
        SimpleBuffer buffer = new SimpleBuffer();
        ITaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 2);
        
        generator.generate();
        
        assertArrayEquals(new int[]{1, 2}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerate3() {
        SimpleBuffer buffer = new SimpleBuffer();
        
        new SimpleTaskGenerator(buffer, -5, 11).generate();
        
        assertArrayEquals(new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithStartValue() {
        SimpleBuffer buffer = new SimpleBuffer();
        ITaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 2);
        
        ((SimpleTaskGenerator) generator).withStartValue(5).generate();
        
        assertArrayEquals(new int[]{5, 6}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithAmount() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskGenerator generator = new SimpleTaskGenerator(buffer, 0, 1);
        
        generator.withAmount(5).generate();
        
        assertArrayEquals(new int[]{0, 1, 2, 3, 4}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithEverything() {
        SimpleBuffer buffer = new SimpleBuffer();
        
        new SimpleTaskGenerator(buffer, 1, 1).withStartValue(-2).withAmount(5).generate();
        
        assertArrayEquals(new int[]{-2, -1, 0, 1, 2}, buffer.get().getData());
    }
    
    
    @Test
    void testGenerateWithNonEmptyBuffer() {
        Task task = new Task(0, 0, 0);
        SimpleBuffer buffer = new SimpleBuffer();
        ITaskGenerator generator = new SimpleTaskGenerator(buffer, 0, 3);
        
        buffer.add(task);
        
        generator.generate();
        
        assertAll(
                () -> assertEquals(task, buffer.get()),
                () -> assertArrayEquals(new int[]{0, 1, 2}, buffer.get().getData())
        );
    }
}
